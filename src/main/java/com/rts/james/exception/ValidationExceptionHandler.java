package com.rts.james.exception;

import com.rts.james.constants.ErrorCode;
import com.rts.james.controller.LoginController;
import com.rts.james.dto.ErrorCodeMessageDto;
import java.util.ArrayList;
import java.util.List;
import javax.validation.UnexpectedTypeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice(assignableTypes = {LoginController.class})
public class ValidationExceptionHandler {

  @ExceptionHandler({BindException.class, MethodArgumentNotValidException.class,
      UnexpectedTypeException.class})
  public ResponseEntity<ErrorCodeMessageDto> handleInvalidArgumentException(Exception ex) {

    List<String> errorMessages = new ArrayList<>();
    if (ex instanceof BindException) {
      for (ObjectError error : ((BindException) ex).getBindingResult()
          .getAllErrors()) {
        errorMessages.add(error.getDefaultMessage());
      }
    }

    String errorMessagesString = String.join("\r\n", errorMessages);

    ErrorCodeMessageDto errorResponseDto = ErrorCodeMessageDto
        .builder()
        .message(errorMessagesString)
        .errorCode(ErrorCode.INVALID_ARGUMENTS)
        .build();

    return new ResponseEntity<>(
        errorResponseDto, HttpStatus.BAD_REQUEST);
  }
}
