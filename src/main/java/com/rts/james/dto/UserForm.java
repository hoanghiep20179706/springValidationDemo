package com.rts.james.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class UserForm {

  @NotNull
  @NotEmpty
  @Size(min = 2, max = 50, message = "Invalid size name")
  private String name;

  @NotNull
  @NotEmpty
  private String password;

  @NotNull
  @NotEmpty
  @Email(message="Invalid email format")
  private String email;
}