package com.rts.james.dto;

import com.rts.james.constants.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
@AllArgsConstructor
public class ErrorCodeMessageDto {
  private String message;
  private ErrorCode errorCode;
}
