package com.rts.james.controller;

import com.rts.james.dto.UserForm;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@Validated
public class LoginController {

  @GetMapping("/login")
  public String login() {
    return "login";
  }

  @GetMapping("/result")
  public String result() {
    return "result";
  }

  @PostMapping("/login")
  public String greetingSubmit(@Valid UserForm userForm, Model model, BindingResult bindingResult) {
    if (bindingResult.hasErrors()) {
      return "login";
    }
    model.addAttribute("userLogged", userForm);
    return "result";
  }

}