<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>View Users</title>
    <link href="<c:url value="/resources/main.css" />" rel="stylesheet">
</head>
<body>
<div class="wrapper fadeInDown">
    <div id="formContent">
        <!-- Tabs Titles -->
        <h2 class="inactive underlineHover">Sign Up </h2>

        <!-- Login Form -->
        <form modelAttribute="user" action="#" th:action="@{/login}" th:object="${userForm}"
              method="post">
            <input type="text" id="login" class="fadeIn second" th:field="*{name}"
                   name="name" placeholder="login">
            <input type="password" id="password" class="fadeIn third" th:field="*{password}"
                   name="password" placeholder="****">
            <input type="text" id="email" class="fadeIn third" th:field="*{email}"
                   name="email" placeholder="email">
            <input type="submit" class="fadeIn fourth" value="Log In">
        </form>

        <!-- Remind Passowrd -->
        <div id="formFooter">
            <a class="underlineHover" href="#">Forgot Password?</a>
        </div>

    </div>
</div>
</body>
</html>